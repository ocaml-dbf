value revert_table tbl =
  let res = String.make 128 '?'
  in (
    for i = 128 to 255 do
     (
      let q = (Char.code tbl.[i-128])
      in
        if q >= 128
        then
          res.[q - 128] := Char.chr i
        else
          ()
     )
     done
    ; res
  )
;


value recode_upper tbl s =
  let l = String.length s
  and res = String.copy s
  in do
    {
    for i = 0 to (l-1) do
      {
      let c = Char.code res.[i]
      in
        if c >= 128
        then
          res.[i] := tbl.[c-128]
        else
          ()
      }
    ; res
    }
;


value tbl_cp_866_to_1251 =
"\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf\
\xd0\xd1\xd2\xd3\xd4\xd5\xd6\xd7\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf\
\xe0\xe1\xe2\xe3\xe4\xe5\xe6\xe7\xe8\xe9\xea\xeb\xec\xed\xee\xef\
---\xa6+\xa6\xa6\xac\xac\xa6\xa6\xac---\xacL+T+-+\xa6\xa6L\xe3\
\xa6T\xa6=+\xa6\xa6TTLL-\xe3++----\xa6\xa6-\xf0\xf1\xf2\xf3\xf4\
\xf5\xf6\xf7\xf8\xf9\xfa\xfb\xfc\xfd\xfe\xff\xa8\xb8\xaa\xba\xaf\
\xbf\xa1\xa2\xb0\x95\xb7v\xb9\xa4\xa6\xa0"
;
assert (String.length tbl_cp_866_to_1251 = 128);

value tbl_cp_1251_to_866 =
"__\x27_\x22:\xc5\xd8_%_<_____\x27\x27\x22\x22\x07--_T_>____\
\xff\xf6\xf7_\xfd_\xb3\x15\xf0c\xf2<\xbf-R\xf4\xf8+___\xe7\x14\
\xfa\xf1\xfc\xf3>___\xf5\x80\x81\x82\x83\x84\x85\x86\x87\x88\
\x89\x8a\x8b\x8c\x8d\x8e\x8f\x90\x91\x92\x93\x94\x95\x96\x97\
\x98\x99\x9a\x9b\x9c\x9d\x9e\x9f\xa0\xa1\xa2\xa3\xa4\xa5\xa6\
\xa7\xa8\xa9\xaa\xab\xac\xad\xae\xaf\xe0\xe1\xe2\xe3\xe4\xe5\
\xe6\xe7\xe8\xe9\xea\xeb\xec\xed\xee\xef"
;
assert (String.length tbl_cp_1251_to_866 = 128);



type cvtr = { importer : string -> string; exporter : string -> string };

(*
value mkpair tbl =
let  _ = Printf.printf "TBL=\"%s\"\n" (revert_table tbl)
in
  { importer = recode_upper tbl
  ; exporter = recode_upper (revert_table tbl)
  }
;
*)


value default_converter = (* mkpair tbl_cp_866_to_1251 *)
  { importer = recode_upper tbl_cp_866_to_1251
  ; exporter = recode_upper tbl_cp_1251_to_866
  }
;

