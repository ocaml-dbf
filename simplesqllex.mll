{
open Simplesqlparse
;;

let rec undq s =
  try
    let i = String.index s '\'' in
    let () = assert (s.[i+1] = '\'') in
    (String.sub s 0 (i+1)) ^
    (undq
       (String.sub s (i+2)
          ((String.length s) - (i+2))
       )
    )
  with
    Not_found -> s
;;

let string_of_sqltoken t =
  match t with
    Ident s -> Printf.sprintf "Ident(%S)" s
  | Str s -> Printf.sprintf "Str(%S)" s
  | Num s -> Printf.sprintf "Num(%S)" s
  | LParen -> "LParen"
  | RParen -> "RParen"
  | Comma  -> "Comma"
  | Sep    -> "Sep"
  | Eof    -> "Eof"
  | Create -> "Create"
  | Table -> "Table"
  | Insert -> "Insert"
  | Into -> "Into"
  | Values -> "Values"
  | Ignore_token -> Printf.sprintf "Ignore_token"
;;

let kwht = Hashtbl.create 29
;;

List.iter
  (fun (text, kw) ->
     Hashtbl.add kwht (String.uppercase text) kw
  )
  [ ("create", Create )
  ; ("table",  Table  )
  ; ("insert", Insert )
  ; ("into",   Into   )
  ; ("values", Values )
  ; ("commit", Ignore_token)
  ]
;;

let kw_or_ident text =
  try Hashtbl.find kwht (String.uppercase text)
  with Not_found -> Ident text
;;

}


let ident_head = ['A'-'Z' 'a'-'z' '_']
let dig = ['0'-'9']
let ident_body = ident_head | dig | ['.' '#' '$']
let eol = [ '\n' '\r' ]
let no_eol = [^ '\n' '\r' ]
let spc = [ ' ' '\t' ]
let eol_eof = eol | eof


rule sqltoken = parse
  eof                            { Eof }
| (ident_head ident_body*) as i  { kw_or_ident i }
| '"'                            { Ident (dblquoted lexbuf) }
| '\''                           { Str (singlequoted lexbuf) }
| ( spc*
    (
     ( (['-' '+'] | spc*)?
       ( ( dig+ (('.' dig*)?) )
       | ('.' dig+)
       )
     ) as n
    )
  )                         { Num n }
| spc* eol+ spc* '/' spc* eol+   { Sep }
| ( spc | eol )+                 { sqltoken lexbuf }
| '(' { LParen }
| ')' { RParen }
| ',' { Comma }
| (';' spc*)+ { Sep }
| "--" no_eol* eol_eof  { sqltoken lexbuf }

 and dblquoted = parse
  ([^ '"']* as i) '"'   { i }
| _  { failwith "double-quoted string not terminated" }

 and singlequoted = parse
  ( ( ( [^ '\'']* )
      ( ( "''" [^ '\'']* )* )
    ) as i) '\''  { undq i }
| _  { failwith "single-quoted string not terminated" }
