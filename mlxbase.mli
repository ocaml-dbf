(** OCaml xBase *)

(** type of column in dbf file *)
type col_type =
 [ CT_Character | CT_Number | CT_Logical | CT_Date | CT_Memo | CT_Float
 | CT_Binary | CT_General | CT_Picture | CT_Currency | CT_Datetime
 | CT_Integer | CT_Varifield | CT_VariantX | CT_Timestamp | CT_Double
 | CT_Autoincrement | CT_NullFlags
 ]
;


(** column specification *)
type col_spec =
 { col_name : string
 ; col_type : col_type
 ; field_size : int
 ; field_decim : int
 }
;


exception Bad_format of string;

(** abstract type for reading dbf files *)
type dbf_reader
;

type xbase_hint = 
  [ IgnoreDeletedFlag 
  ]
  (** Ignore the "deleted record" flag, which specifies whether 
                          each particular data record is deleted or not.

                          Try it if you get "bad deleted_flag" exceptions.
                          *)
;

(** [open_reader ~decode filename] opens dbf file [filename] for reading.
    Every field's value will be decoded with [decode] function (default
    [decode] doesn't modify the values).
*)
value open_reader : ?decode:(string -> string) -> ?hints:list xbase_hint -> string -> dbf_reader
;


value get_reader_codepage : dbf_reader -> string
;


(** [reader_with_decoder new_decoder dbf_reader] returns [dbf_reader] with
    new decoder.
*)
value reader_with_decoder : (string -> string) -> dbf_reader -> dbf_reader
;


(** reads one record from dbf.
    @raise End_of_file on end of file
    @raise Bad_format on bad format

    may raise other input exceptions.
 *)
value read_record : dbf_reader -> list string
;

value close_reader : dbf_reader -> unit
;

value colspecs_of_reader : dbf_reader -> list col_spec
;

(** @return codepage in textual representation.
    warning: for unknown codepages the return value looks like
    "language_driver_0x%02x", which is not acceptable for
    [open_writer].

    todo: represent codepage as sum type like
    [ CP_866 | CP_1251 | ... | Unknown of int ]
*)
value codepage_of_reader : dbf_reader -> string
;


(** abstract type for writing dbf files *)
type dbf_writer
;

(** [open_writer ~encode filename col_specs codepage]
    For now, only these codepages can be written into dbf header:
    "437", "850", "1252", "852", "865", "866", "437G", "1250", "1251".
    Every field's value will be encoded with [encode] function (default
    [encode] doesn't modify the values).
*)
value open_writer :
   ?encode:(string -> string)
   -> string -> list col_spec -> string ->
   dbf_writer
;

value write_record : dbf_writer -> list string -> unit
;

value close_writer : dbf_writer -> unit
;

value char_of_coltype : col_type -> char
;


(** tries to fix broken dbf *)
value dbf_fix : string -> unit
;
