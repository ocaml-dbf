
.PHONY: doc build clean

build:
		./bld.sh

doc:
		ocamlbuild mlxbase.docdir/index.html

clean:
		./clean.sh
