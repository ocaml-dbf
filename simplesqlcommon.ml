type sqlexpr =
  [ SENum of string and string
  | SEStr of string
  | SECall of string and list sqlexpr
  | SEIdent of string
  ]
;


type sqlcmd =
  [ Create_table of string and list sqlcolspec
  | Insert of string and list string and list sqlexpr
  | Ignore_command
  ]
 and sqlcolspec = (string * string * option (int * int))  (* name, type, wid *)
;


value rtrim s =
  let rec inner i =
    if i = -1
    then ""
    else
      if s.[i] = ' '
      then
        inner (i-1)
      else
        String.sub s 0 (i + 1)
  in
    inner (String.length s - 1)
;

value ltrim s =
  let slen = String.length s
  in
  let rec inner i =
    if i = slen
    then ""
    else
      if s.[i] = ' '
      then
        inner (i+1)
      else
        String.sub s i (slen - i)
  in
    inner 0
;


value lrtrim s = ltrim (rtrim s)
;


value looks_like_number s =
  let len = String.length s in
  if len = 0
  then False
  else
    inner 0
    where rec inner i =
      if i = len then True
      else
      let c = s.[i] in
      if ((c >= '0' && c <= '9') || (i = 0 && (c = '-' || c = '+')))
      then inner (i+1)
      else False
;


value string_exists p s =
  let len = String.length s in
  inner 0
  where rec inner i =
    if i = len
    then False
    else
      if p s.[i]
      then
        True
      else
        inner (i+1)
;


value looks_like_frac s =
     (s = "")
  || (not (string_exists (fun c -> c < '0' || c > '9') s))
;


value split_number s =
  let (n_int, n_frac) =
    try
      let dot =
        try
          String.index s '.'
        with
        [ Not_found -> String.index s ',' ]
      in
      ( (String.sub s 0 dot)
      , (String.sub s (dot+1) ((String.length s) - dot - 1) )
      )
    with
    [ Not_found -> (s, "") ]
  in
  let ((n_int, n_frac) as npair) = (lrtrim n_int, lrtrim n_frac)
  in
(*
  let () = Printf.printf "npair = %S , %S\n" n_int n_frac in
*)
  let fail () =
    failwith (Printf.sprintf "value %S doesn't look like number" s)
  in
  match ( n_int, n_frac
        , looks_like_number n_int, looks_like_number n_frac
	) with
  [ ("", "", _, _) -> ("", "")
  | ("", _, _, True) -> ("0", n_frac)
  | (_, "", True, _) -> (n_int, "")
  | (_, _, True, True) -> npair
  | _ -> fail ()
  ]
;


value compare_ident a b =
  Pervasives.compare (String.uppercase a) (String.uppercase b)
;


value (int_of_sqlnum : string -> int) sn =
  int_of_string sn
;


open Printf
;


value string_of_ident i = i
;

value rec string_of_sqlexpr e =
  match e with
  [ SENum a b -> sprintf "Num(%s.%s)" a b
  | SEStr s -> sprintf "Str(%S)" s
  | SECall f el ->
      sprintf
        "Call(%s, (%s))"
        f
        (String.concat ", " (List.map string_of_sqlexpr el))
  | SEIdent s -> sprintf "Ident(%s)" s
  ]
;


value string_of_colspec cs =
  let (nam, ty, optsz) = cs in
  let sz =
    match optsz with
    [ None -> ""
    | Some (wid, dec) -> sprintf "(%i, %i)" wid dec
    ]
  in
    sprintf "%s %s %s" nam ty sz
;


value string_of_sqlcmd sc =
  match sc with
  [ Create_table n cs ->
      sprintf "Create_table(%s,%s);\n"
        n
        (String.concat "" (List.map (fun s -> sprintf "  %s\n" (string_of_colspec s)) cs))
  | Insert n cs vs ->
      sprintf "Insert(%s,\n%s\n%s\n);\n"
        n
        (String.concat ", " (List.map string_of_ident cs))
        (String.concat ", " (List.map string_of_sqlexpr vs))
  | Ignore_command -> sprintf "Ignore_command;\n"
  ]
;


value (execute_sql_command_val : ref (sqlcmd -> unit)) =
  ref (fun _ -> failwith "execute_sql_command_val not defined")
;

