open Mlxbase
;

open Simplesqllex
;

open Simplesqlcommon
;


value default_codepage = "866"
;


(*
value rec dump () =
  let t = sqltoken lb in
  let () =
    Printf.printf "%s\n%!" (string_of_sqltoken t)
  in
  if t = Eof
  then ()
  else dump ()
;

dump ();
*)




value xbasetype_of_oratype ot =
  match (String.lowercase ot) with
  [ "varchar2" -> CT_Character
  | "number" -> CT_Number
  | "date" -> CT_Date
  | x -> failwith (Printf.sprintf "unsupported oracle type %S" x)
  ]
;

value xbasecolspec_of_oracolspec ocs =
  let (nam, ty, optsz) = ocs in
  let (fsz, fdc) =
    match optsz with
    [ None -> (0, 0)
    | Some szs -> szs
    ]
  in
  let xty = xbasetype_of_oratype ty
  in
    { col_name = nam
    ; col_type = xty
    ; field_size = if xty = CT_Date then 8 else fsz
    ; field_decim = fdc
    }
;


value writers = Hashtbl.create 5
;


value exec_createtable nam lcs =
  let () = assert (not (Hashtbl.mem writers nam)) in
  let xbspecs = (List.map xbasecolspec_of_oracolspec lcs) in
  let pcs =
    List.fold_left
      (fun acc cs ->
         if (List.mem_assoc cs.col_name acc)
         then
           failwith (Printf.sprintf
             "Column names must be unique (%S)" cs.col_name)
         else
           [ (cs.col_name, (cs.col_type, (cs.field_size, cs.field_decim)))
           :: acc ]
      )
      []
      xbspecs
  in
  let colord = Hashtbl.create (List.length pcs) in
  let () = do
    { inner 0 lcs
      where rec inner i lcs =
        match lcs with
        [ [] -> ()
        | [(nam, _, _) :: tl] ->
            let () = Hashtbl.add colord nam i in
            let () = Printf.printf "%s is %i's column\n" nam i in
            inner (i+1) tl
        ]
    }
  in
  let () = Printf.printf "pcs len = %i\n" (List.length pcs) in
  let w : dbf_writer =
    open_writer
      ~encode:Cpcvt.default_converter.Cpcvt.exporter
      (nam ^ ".dbf")
      xbspecs
      default_codepage
  in
    Hashtbl.add writers nam (w, pcs, colord)
;


value ident_null = "NULL"
;

value ident_is_null i =
  (compare_ident ident_null i) = 0
;


value gente tyexp =
  fun t ->
    failwith (Printf.sprintf "type error: %s expected, %s got" tyexp t)
;


value stringval_of_sqlexpr se =
  let te = gente "string"
  in
  match se with
  [ SEStr s -> s
  | SEIdent i when ident_is_null i -> ""
  | SENum _ -> te "number"
  | SECall _ _ -> te "function call"
  | SEIdent _ -> te "identifier"
  ]
;


value num_parse str =
 let () =
  String.iter
    (fun c ->
       if c >= '0' && c <= '9'
       then ()
       else failwith (Printf.sprintf "number %S contains non-digits" str)
    )
    str
 in
   Some str
;


type tmpdate =
  { dyear : option string
  ; dmon : option string
  ; dday : option string
  }
;


value charlist_of_string s =
  inner [] ((String.length s) - 1)
  where rec inner acc i =
    if i < 0
    then acc
    else inner [s.[i] :: acc] (i - 1)
;


value string_of_charlist cl =
  let r = String.make (List.length cl) ' ' in
  let (_:int) = List.fold_left
    (fun i c -> do { r.[i] := c; i+1 } )
    0
    cl
  in
    r
;


value exec_todate args =
  match args with
  [ [se_datetext; se_dateformat] ->
      match (se_datetext, se_dateformat) with
      [ (SEStr datetext, SEStr dateformat) ->
          let optset ov cl =
            let () = assert (ov = None) in
            Some (string_of_charlist cl)
          and nomatch () =
            failwith (Printf.sprintf
              "string %S doesn't match date format %S"
               datetext dateformat)
          in
          date_of_fmt
            { dyear = None; dmon = None; dday = None }
            (charlist_of_string datetext)
            (charlist_of_string dateformat)
          where rec date_of_fmt acc dt df =
            match (df, dt) with
            [ (['D'; 'D' :: dftl], [d1; d2 :: dttl]) ->
                date_of_fmt
                  {(acc) with dday = optset acc.dday [d1; d2]}
                  dttl dftl
            | (['M'; 'M' :: dftl], [m1; m2 :: dttl]) ->
                date_of_fmt
                  {(acc) with dmon = optset acc.dmon [m1; m2]}
                  dttl dftl
            | (['R'; 'R'; 'R'; 'R' :: dftl], [y1;y2;y3;y4::dttl])
            | (['Y'; 'Y'; 'Y'; 'Y' :: dftl], [y1;y2;y3;y4::dttl]) ->
                date_of_fmt
                  {(acc) with dyear = optset acc.dyear [y1;y2;y3;y4]}
                  dttl dftl
            | (['M'; 'I' :: dftl], [_;_ :: dttl])
            | (['H'; 'H'; '2'; '4' :: dftl], [_;_ :: dttl])
            | (['H'; 'H'; '1'; '2' :: dftl], [_;_ :: dttl])
            | (['H'; 'H' :: dftl], [_;_ :: dttl])
            | (['S'; 'S' :: dftl], [_;_ :: dttl]) ->
                 date_of_fmt acc dttl dftl

            | ([fc :: dftl], [tc :: dttl]) ->
                if fc = tc
                then
                  date_of_fmt acc dttl dftl
                else
                  nomatch ()
            | ([], []) ->
                match (acc.dyear, acc.dmon, acc.dday) with
                [ (Some y, Some m, Some d) ->
                    let () = assert
                      (    ((String.length y) = 4)
                        && ((String.length m) = 2)
                        && ((String.length d) = 2)
                      )
                    in
                      Some (y ^ m ^ d)
                | _ -> failwith "year, month and day must be defined"
                ]

            | ([], _) | (_, []) -> nomatch ()
            ]
      | (SENum _, SENum _) | (SENum _, SEStr _) | (SENum _, SECall _ _)
      | (SENum _, SEIdent _) | (SEStr _, SENum _) | (SEStr _, SECall _ _)
      | (SEStr _, SEIdent _) | (SECall _ _, SECall _ _)
      | (SECall _ _, SENum _) | (SECall _ _, SEStr _)
      | (SECall _ _, SEIdent _) | (SEIdent _, SENum _) | (SEIdent _, SEStr _)
      | (SEIdent _, SECall _ _) | (SEIdent _, SEIdent _)
          -> failwith "invalid argument types for TO_DATE"
      ]
  | [ SEIdent i ] when ident_is_null i -> None
  | _ -> failwith "invalid argument(s) for TO_DATE"
  ]
;


value dateval_of_sqlexpr se =
  let te = gente "date" in
  match se with
  [ SEStr _ -> te "string"
  | SEIdent i ->
      if ident_is_null i
      then None
      else te "identifier"
  | SENum _ -> te "number"
  | SECall funname funargs ->
      if (compare_ident funname "to_date") = 0
      then
        exec_todate funargs
      else
        te "unknown function call"
  ]
;



value lrpad dir padch sz s =
  let slen = String.length s in
  let () = assert (sz >= slen) in
  if slen = sz
  then s
  else
    let padding = (String.make (sz - slen) padch) in
    if dir
    then padding ^ s
    else s ^ padding
;


value lpad = lrpad True
  and rpad = lrpad False
;


value rec optnumval_of_sqlexpr se =
  let te = gente "number"
  in
  match se with
  [ SEStr str ->
      if str = ""
      then None
      else Some (split_number str)
(*
      failwith
        (Printf.sprintf
         "optnumval_of_sqlexpr: ᤥ���� ⠪� ����� : string -> option (string * string) ��� %S."
         str
        )
*)
(*
      if  .... ᤥ���� ⠪� ����� : string -> option (string * string)
      then Some str
      else failwith (Printf.sprintf
        "type error: number expected, string %S got" str
      )
*)
  | SEIdent i when ident_is_null i -> None
  | SECall funname [funarg]
    when (compare_ident funname "to_number") = 0
    ->
      optnumval_of_sqlexpr funarg
  | SECall _ _ ->
        te "unknown function call"
  | SEIdent _ -> te "identifier"
  | SENum a b -> Some (a, b)
  ]
;


value xbase_format_value ty (field_size, field_decim) v =
  match ty with
  [ CT_Character ->
      let sv = stringval_of_sqlexpr v in
      let sv_len = String.length sv in
      if sv_len > field_size
      then failwith "string value does not fit column"
      else
        rpad ' ' field_size sv
  | CT_Number ->
      let osv = optnumval_of_sqlexpr v in
      match osv with
      [ None -> lpad ' ' field_size ""
      | Some (n_int, n_frac) ->
          if String.length n_frac > field_decim
          then failwith (Printf.sprintf
            "number %s has more than %i fractional digits"
            (string_of_sqlexpr v) field_decim)
          else
            if field_decim = 0
            then
              lpad ' ' field_size n_int
            else
                (lpad ' ' (field_size - field_decim - 1) n_int)
              ^ "."
              ^ (rpad '0' field_decim n_frac)
      ]
  | CT_Date ->
      match (dateval_of_sqlexpr v) with
      [ None -> lpad ' ' 8 ""
      | Some v -> v
      ]
  | CT_Logical | CT_Memo | CT_Float | CT_Binary | CT_General
  | CT_Picture | CT_Currency | CT_Datetime | CT_Integer 
  | CT_Varifield | CT_VariantX | CT_Timestamp | CT_Double
  | CT_Autoincrement | CT_NullFlags ->
      failwith "field type not supported"
  ]
;


value exec_insert nam cols vals =
  let (w, pcs, ord) =
    try Hashtbl.find writers nam
    with
    [ Not_found -> failwith "table must be created in this 'session'"
    ]
  in
  let pcs_count = List.length pcs in
  let () = assert ((List.length cols) = (List.length vals)) in
  let () = Printf.printf "cols_count = %i\n" pcs_count in
  let xvals = Array.make pcs_count None in
  let setval colname v =
    let i = Hashtbl.find ord colname in
    let () = Printf.printf "setval ord: %i / %i\n" i (Array.length xvals) in
    let () = assert (xvals.(i) = None) in
    xvals.(i) := Some (v)
  in
  let () =
    inner cols vals
    where rec inner cols vals =
      match (cols, vals) with
      [ ([], []) -> ()
      | ([], _) | (_, []) -> assert False
      | ([colname::colstl], [v::valstl]) ->
          let (ty, wid) = List.assoc colname pcs in
          let fv =
            try xbase_format_value ty wid v
            with
            [ e -> failwith (Printf.sprintf
               "error formatting value for column %s: %s"
               colname (Printexc.to_string e))
            ]
          in
          let () = setval colname fv in
          inner colstl valstl
      ]
  in
  let xbase_vals =
    Array.fold_right
      (fun optv acc ->
         match optv with
         [ None ->
             let () = print_string "insert: value for column not specified\n"
             in [("") :: acc]
         | Some v -> [v :: acc]
         ]
      )
      xvals
      []
  in
    write_record w xbase_vals
;


value exec_cmd cmd =
  let () = Printf.printf "%s\n===============\n%!" (string_of_sqlcmd cmd) in
  match cmd with
  [ Create_table nam lcs -> exec_createtable nam lcs
  | Insert nam cols vals -> exec_insert nam cols vals
  | Ignore_command -> ()
  ]
;

(*
value cmdlist_of_file exec_cmd fn =
  let lb = Lexing.from_channel (open_in_bin fn) in
  Simplesqlparse.main Simplesqllex.sqltoken lb
;
value exec_cmdlist = List.iter
  (fun cmd -> do
    ; exec_cmd cmd
    }
  )
;
*)

value () =
  Simplesqlcommon.execute_sql_command_val.val := exec_cmd
;


value exec_file fn =
(*
  exec_cmdlist (cmdlist_of_file fn)
*)
  let lb = Lexing.from_channel
    (if fn = "-"
     then do { set_binary_mode_in stdin True; stdin }
     else open_in_bin fn
    ) in
  while (not (Simplesqlparse.sqlcmdsep Simplesqllex.sqltoken lb))
  do { () }
;


value close_writers () =
  Hashtbl.iter (fun _ (w, _, _) -> close_writer w) writers
;


value import files = do
  { List.iter exec_file files
  ; close_writers ()
  }
;


value escape_string s =
  let b = Buffer.create (10 + String.length s)
  in do
   { Buffer.add_char b '\''
   ; String.iter
      ( fun c ->
        match c with
        [ '\'' -> Buffer.add_string b "\'\'"
        | x -> Buffer.add_char b x
        ]
      )
      s
   ; Buffer.add_char b '\''
   ; Buffer.contents b
   }
;


value numsizes cs = do
  { assert (cs.col_type=CT_Number)
  ; if cs.field_decim = 0
    then
      (cs.field_size, 0)
    else
      (cs.field_size - cs.field_decim - 1, cs.field_decim)
  }
;


value escape_number cs =
  let (bef, aft) = numsizes cs in
  let escafter =
       ", "
     ^ escape_string
         (
           (String.make bef '9')
         ^ (if aft = 0
            then
              ""
            else
              "." ^ (String.make aft '9')
           )
         )
     ^ ")"
  in
  fun n ->
    if n = ""
    then "to_number(NULL)"
    else "to_number(" ^ (escape_string n) ^ escafter
;


value guess_century y = if y < "70" then "20" else "19";

value string_forall pred s =
  let len = String.length s in
  inner 0
  where rec inner i =
    (i = len) || ((pred s.[i]) && (inner (i+1)))
;


value alldigits = string_forall (fun c -> c>='0' && c<='9')
;


value escape_date d =
  if d = ""
  then
    "to_date(NULL)"
  else
    let bdf reason =
      failwith (Printf.sprintf "Bad data format (%s)" reason)
    in
    let canondate =
      match (String.length d) with
      [ 6 (* YYMMDD *) -> (guess_century (String.sub d 0 2)) ^ d
      | 8 (* YYYYMMDD *) -> d
      | x -> bdf (Printf.sprintf "length=%i" x)
      ]
    in
    if not (alldigits canondate)
    then
      bdf "date must consist only of digits"
    else
      "to_date(" ^ (escape_string canondate) ^ ", 'YYYYMMDD')"
;


value rtrim_hof f = fun x -> f (rtrim x);

value ltrim_hof f = fun x -> f (ltrim x);

value export_int32 str =
  ( assert (String.length str = 4)
  ; let byte i = Int32.shift_left (Int32.of_int (Char.code str.[i])) (i*8) in
    let r =
      Int32.add (byte 0) (
      Int32.add (byte 1) (
      Int32.add (byte 2) (
                (byte 3) ))) in
    Int32.to_string r
  )
;

value mk_escape_func cs =
 match cs.col_type with
 [ CT_Character -> rtrim_hof escape_string
 | CT_Number -> rtrim_hof (ltrim_hof (escape_number cs))
 | CT_Date -> rtrim_hof (ltrim_hof escape_date)
 | CT_Integer -> export_int32

 | CT_Logical | CT_Memo | CT_Float        
 | CT_Binary | CT_General | CT_Picture | CT_Currency | CT_Datetime
 | CT_Varifield | CT_VariantX | CT_Timestamp | CT_Double
 | CT_Autoincrement | CT_NullFlags
     -> failwith (Printf.sprintf 
      "only character, number and date field types are supported (field '%c')"
      (char_of_coltype cs.col_type)
      )
 ]
;


value is_alpha c =
     (c >= 'A' && c <= 'Z')
  || (c >= 'a' && c <= 'z')
;


value name_need_escape s =
     (not (is_alpha s.[0]))
  || (string_exists
        (fun c ->
           not
             (   (is_alpha c)
              || (c >= '0' && c <= '9')
              || c = '.' || c = '_' || c = '$' || c = '#'
             )
        )
        s
     )
;


value escape_name s =
  if name_need_escape s
  then "\"" ^ s ^ "\""
  else s
;


type skip_info = [= `Use | `Skip of string ]
;

value skip_mask colspecs : list skip_info =
    (List.map
       (fun cs ->
          if cs.col_type = CT_Memo
          then `Skip "MEMO"
          else if cs.col_type = CT_NullFlags
          then `Skip "_NullFlags"
          else `Use
       )
       colspecs
    )
;


value rec list_map3 f a b c =
  match (a,b,c) with
  [ ([],_,_) | (_,[],_) | (_,_,[]) -> []
  | ([ah::at], [bh::bt], [ch::ct]) ->
      [(f ah bh ch) :: (list_map3 f at bt ct)]
  ]
;


value export_iter_dbf_to_sql tblname colspecs =
  let skip_mask = skip_mask colspecs in
  let ins_pre_values =
    Printf.sprintf "insert into %s (%s) values \n ("
      (escape_name tblname)
      (String.concat
         ", "
         (List.flatten
            (List.map2
               (fun cs sk ->
                  match sk with
                  [ `Skip _reason -> []
                  | `Use -> [ escape_name cs.col_name ]
                  ]
               )
               colspecs
               skip_mask
            )
         )
      )
  and ins_post_values = ");\n"
  and escape_funcs =
     List.map2
       (fun cs sk ->
          match sk with
          [ `Skip _reason -> fun _ -> assert False
          | `Use -> mk_escape_func cs
          ]
       )
       colspecs
       skip_mask
  in
    fun fieldlist ->
    do
      { print_string ins_pre_values
      ; print_string
         (String.concat ", "
           (List.flatten
              (list_map3
                (fun escf v sk ->
                   match sk with
                   [ `Skip _reason -> []
                   | `Use -> [ escf v ]
                   ]
                )
                escape_funcs
                fieldlist
                skip_mask
              )
           )
         )
      ; print_string ins_post_values
      }
;


value export_dbf_to_sql filename tblname =
do
  { Printf.printf "/* export dbf %s into table %s */\n" filename tblname
  ; let d = open_reader filename in
    let d =
      match get_reader_codepage d with
      [ "866" | "865" ->
          reader_with_decoder Cpcvt.default_converter.Cpcvt.importer d
      | _ -> d
      ]
    in
    let cs = colspecs_of_reader d in
    let iterfunc = export_iter_dbf_to_sql tblname cs in
    try do
      { Printf.printf "/* dbf codepage: %s */\n" (codepage_of_reader d)
      ; while True do
          { let r = read_record d
            in
              iterfunc r
          }
      }
    with
    [ End_of_file -> do
        { close_reader d
        ; print_string "commit;\n"
        }
    | x -> do
        { close_reader d
        ; raise x
        }
    ]
  }
;


value ddl_of_colspec cs =
 (escape_name cs.col_name) ^ " " ^ type_ddl
where type_ddl =
 match cs.col_type with
 [ CT_Character -> Printf.sprintf "varchar2(%d)" cs.field_size
 | CT_Number ->
     let (bef, aft) = numsizes cs
     in
       Printf.sprintf "number(%d,%d)" bef aft
 | CT_Date -> "date"
 | CT_Integer -> "number(10)"  (* 4 bytes *)
 
 | CT_Logical | CT_Memo | CT_Float        
 | CT_Binary | CT_General | CT_Picture | CT_Currency | CT_Datetime
 | CT_Varifield | CT_VariantX | CT_Timestamp | CT_Double       
 | CT_Autoincrement | CT_NullFlags
     -> failwith "only character, number and date field types are supported"
 ]
;


value skip_fields colspecs =
  List.flatten
    (List.map2
       (fun cs sk ->
          match sk with
          [ `Use -> [ cs ]
          | `Skip reason ->
               ( Printf.eprintf
                   "warning: skipping %s-column %S\n%!"
                   reason cs.col_name
               ; []
               )
          ]
       )
       colspecs
       (skip_mask colspecs)
    )
;


value extract_ddl filename tblname =
do
  { Printf.printf "/* ddl for dbf %s (table name %s) */\n" filename tblname
  ; let d = open_reader
      ~decode:Cpcvt.default_converter.Cpcvt.importer
      filename
    in do
      { Printf.printf "create table %s\n ( %s\n )\n/\ncomment on table %s is %s\n/\n"
          tblname
          (String.concat "\n , "
            (List.map
               ddl_of_colspec
               (skip_fields (colspecs_of_reader d))
            )
          )
          tblname
          (escape_string (Printf.sprintf "Created by dbfwork from %s" filename))
      }
  }
;


value dbf_fix dbfname =
  Mlxbase.dbf_fix dbfname
;


value main () =
  match List.tl (Array.to_list Sys.argv) with
  [ ["export"; dbfname; tblname] ->
      export_dbf_to_sql dbfname tblname
  | ["ddl"; dbfname; tblname] ->
      extract_ddl dbfname tblname
  | ["import" :: files] ->
      import files
  | ["fix"; dbfname] ->
      dbf_fix dbfname
  | _ ->
      Printf.eprintf
        "usage: %s export <dbffile.dbf> table_name > insert-script.sql\n\
        \    or %s ddl <dbffile.dbf> table_name > ddl-script.sql\n\
        \    or %s import <file.sql> [<file.sql> ...]\n\
        \    or %s fix <dbffile.dbf>\n"
         Sys.argv.(0) Sys.argv.(0) Sys.argv.(0) Sys.argv.(0)
  ]
;

main ();
