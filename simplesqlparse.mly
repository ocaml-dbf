%{
open Simplesqlcommon
;;


%}

%token Ignore_token
%token <string> Ident
%token <string> Str
%token <string> Num
%token LParen RParen Comma Sep Eof Create Table Insert Into Values 

%start sqlcmdsep
%type <bool> sqlcmdsep

%%

sqlcmdsep:
  sqlcmd Sep
    { !Simplesqlcommon.execute_sql_command_val $1
    ; false
    }
| sqlcmd Eof
    { !Simplesqlcommon.execute_sql_command_val $1
    ; true
    }
| Eof
    { true }
;

sqlcmd:
  Create Table Ident LParen colspeclist RParen
    { Create_table ($3, $5) }
| Insert Into Ident LParen identlist1 RParen Values LParen sqlexprlist RParen
    { Insert ($3, $5, $9) }
| Ignore_token
    { Ignore_command }
;

colspeclist:
  colspec Comma colspeclist
    { $1 :: $3 }
| colspec
    { [ $1 ] }
;

colspec:
  Ident Ident optcolspecsize
    { ($1, $2, $3) }
;

optcolspecsize:
    { None }
| colspecsize
    { Some $1 }
;

colspecsize:
  LParen Num RParen
    { ((int_of_sqlnum $2), 0) }
| LParen Num Comma Num RParen
    { ((int_of_sqlnum $2), (int_of_sqlnum $4)) }
;

identlist1:
  Ident Comma identlist1
    { $1 :: $3 }
| Ident
    { [ $1 ] }
;

sqlexprlist:
  sqlexpr Comma sqlexprlist
    { $1 :: $3 }
| sqlexpr
    { [ $1 ] }
;

sqlexpr:
  Ident
    { SEIdent $1 }
| Num
    { let (a, b) = split_number $1 in SENum (a, b) }
| Str
    { SEStr $1 }
| Ident LParen sqlexprlist RParen
    { SECall ($1, $3) }
;

