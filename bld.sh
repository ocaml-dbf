#! /usr/bin/env bash

LIBS=$(echo mlxbase.cm{a,xa})  # to expand {}.

rm -f $LIBS && \
ocamlbuild -j 0 $LIBS dbfwork.{native,byte} && \
{
for L in $LIBS; do
  B="_build/$L"
  if [[ -f "$B" ]] && [[ ! -f "$L" ]];
  then
    ln -s "$B" "$L"
  fi;
done;
}
